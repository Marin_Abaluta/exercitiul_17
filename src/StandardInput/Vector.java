package StandardInput;

import java.io.IOException;
import java.util.Scanner;

public class Vector {

    public static void main(String[] args) throws IOException {

        Scanner in = new Scanner(System.in);
        System.out.print("Introduce the number of elements n = ");
        int n = in.nextInt();

        int[] vector = new int[n];


        for (int i = 0; i < n; i++) {

            System.out.print("V[" + i + "]= ");
            int element = in.nextInt();
            vector[i] = element;

        }

        for (int i = 0; i < n; i++) {

            System.out.println(vector[i]);
        }


    }
}
